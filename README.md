# CentJS

CentJS is an extremely small, extensible DOM utility function. At its most basic, it wraps `Element.querySelectorAll` and uses ES2015's `Proxy` to attempt to dispatch any calls or assignments to the entire result set.

**CentJS is not at all feature-compatible with jQuery, cash, or any other common library that defines `$(...)`.**

# Extensions

## Basic

The basic extensions add `each(fn)`, `find(selector)`, and `slice(...)`, which behave like jQuery/cash's equivalents.

## DOM

The DOM extensions add `clone()` and `appendTo(nodes)`, which behave like jQuery/cash's equivalents.

## Populate

The populate extensions add `populate(locals)`. If `locals` is an array, `populate` will treat the contents of the target

### `populate(locals, fn)`

If `locals` is an array, *populate* operates on the first target node and treats the contents of that node as a template, cloning, populating, and appending the template to the target.

Otherwise, if `locals` is an object, *populate*:

  * Calls `fn` with the target node(s) and the locals.
    * If *fn* returns false, *populate* returns immediately.
  * Finds elements with `@data-property` attributes and replaces their inner text with the specified property (from locals).
  * Finds elements with `@data-condition` attributes and hides the element if the specified condition (evaluated on *locals*) fails.
  * Finds elements with `@data-expression` attributes and replaces their inner text with the result of the specified expression (evaluated on *locals*).
