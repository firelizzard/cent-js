import { CentExtension, cent } from './cent';
import { CentProxy, CentProxyHandler, CentProxyable } from './proxy';

export interface CentDOM {
    appendTo(node: string|CentProxyable): CentProxy
    clone(): CentProxy
}

export default class DOMOverrides extends CentExtension implements CentDOM {
    appendTo(node) {
        const proxy: CentProxy = this as any
        const target = cent(node)
        proxy.nodes.forEach(x => target.appendChild(x))
        return proxy
    }

    clone() {
        const proxy: CentProxy = this as any
        return CentProxyHandler.create(proxy.nodes.map(x => x.cloneNode(true)))
    }
}