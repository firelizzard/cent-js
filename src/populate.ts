import { CentExtension, cent } from './cent';
import { CentProxy, CentProxyHandler } from './proxy';

import { default as basics, CentBasics } from './basics';
import { default as dom, CentDOM } from './dom';

basics.register()
dom.register()

export interface CentPopulate {
    populate(locals, fn?: (CentProxy, any) => any): CentProxy
}

type ExtendedProxy = CentProxy & CentBasics & CentDOM & CentPopulate

export default class PopulateOverrides extends CentExtension implements CentPopulate {
    populate(locals, fn) {
        const proxy: ExtendedProxy = this as any
        const el: Node = proxy.slice(0, 1)

        if (locals instanceof Array) {
            const template = cent(el.childNodes) as any
            template.remove()

            locals.forEach(x => template
                .clone()
                .populate(x, fn)
                .appendTo(el))

            return proxy
        }

        if (fn && fn(prompt, locals) === false)
            return proxy

        proxy.find('[data-property]').each(target => {
            target.innerText = get(locals, target.dataset.property)
        })

        proxy.find('[data-condition]').each(target => {
            const v = evaluate(target.dataset.condition, locals)
            if (!v) target.style.display = 'none'
        })

        proxy.find('[data-expression]').each(target => {
            target.innerText = evaluate(target.dataset.expression, locals)
        })
    }
}

function get(target, path: string|string[]) {
    if (typeof path === 'string')
        path = path.split('.')

    path.forEach(prop => {
        target = target[prop]
    })

    return target
}

function evaluate(expression: string, locals) {
    const fn = Function('locals', `with (locals) { return (${expression}) }`)
    return fn(locals)
}