import { isCent } from './const';
import { CentProxyHandler, CentProxy, CentProxyOverrides } from './proxy';

export interface Cent {
    (...args): CentProxy

    override(more): void
}

const registered = new Map()
export class CentExtension {
    static register() {
        if (registered.has(this))
            return
        cent.override(this.prototype)
        registered.set(this, true)
    }
}

export const cent = <Cent> function cent(...args): CentProxy {
    if (args.length === 1) {
        const [arg] = args

        if (typeof arg === 'object' && arg[isCent])
            return arg

        if (arg instanceof Node || arg instanceof NodeList)
            return CentProxyHandler.create(arg);

        if (arg instanceof Array)
            return CentProxyHandler.join(...arg.map(x => cent(x).nodes))

        switch (typeof arg) {
            case 'string': {
                if (arg.startsWith('<')) {
                    const parser = new DOMParser()
                    const doc = parser.parseFromString(arg, 'text/html')
                    return CentProxyHandler.create(doc.rootElement)
                } else {
                    const els = document.querySelectorAll(arg)
                    return CentProxyHandler.create(els)
                }
            }
        }
    }
}

cent.override = x => CentProxyOverrides.register(x)