import { CentExtension } from './cent';
import { CentProxy, CentProxyHandler } from './proxy';

export interface CentBasics {
    each(fn: (el: Node, i: Number) => any): CentProxy
    find(selector: string): CentProxy
    slice(...args): CentProxy
}

export default class BasicOverrides extends CentExtension implements CentBasics {
    each(fn) {
        const proxy: CentProxy = this as any
        proxy.nodes.forEach((x, i) => fn.call(x, x, i))
        return proxy
    }

    find(selector) {
        const proxy: CentProxy = this as any
        return CentProxyHandler.join(...proxy.nodes
            .filter(x => x instanceof Element)
            .map((x: Element) => x.querySelectorAll(selector))
        )
    }

    slice(...args) {
        const proxy: CentProxy = this as any
        return CentProxyHandler.create(proxy.nodes.slice(...args))
    }
}