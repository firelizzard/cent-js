import { isCent } from './const';

export interface CentProxy extends Node {
    nodes: Node[]
    length: number
    [i: number]: Node
    [x: string]: any
}

export type CentProxyable = CentProxy|NodeList|Node|Node[]|NodeListOf<any>

export class CentProxyHandler implements ProxyHandler<Node[]> {
    static create(els: CentProxyable): CentProxy {
        if (els[isCent])
            return els as any

        if (els instanceof NodeList)
            return this.create(Array.prototype.slice.call(els))

        if (els instanceof Node)
            return this.create([els])

        return new Proxy(els, new this()) as any
    }

    static join(...cents: CentProxyable[]) {
        return this.create([].concat(...cents))
    }

    overrides = new CentProxyOverrides()

    get(target: Node[], property: string|symbol, receiver) {
        if (property === 'nodes')
            return target

        if (property === 'length')
            return length

        if (typeof property === 'number' && property in target)
            return target[property]

        if (property in this.overrides) {
            const val = this.overrides[property]

            if (typeof val !== 'function')
                return val

            return val.bind(receiver)
        }

        if (canCallAll(HTMLElement.prototype, property))
            return callAll;

        if (target.length === 0)
            return void 0;

        if (canCallAll(Object.getPrototypeOf(target[0]), property))
            return callAll;

        return target[0][property];

        function callAll(...args) {
            let r
            target.forEach(x => {
                r = x[property](...args)
            })
            return r
        }
    }

    set(target: Node[], property: string|symbol, value, receiver): boolean {
        if (target.length == 0)
            return false

        try {
            target.forEach(x => {
                x[property] = value;
            })
            return true
        } catch (error) {
            return false
        }
    }
}

export class CentProxyOverrides {
    static register(more) {
        Object.assign(this.prototype, more)
    }

    [isCent]: true
}

function canCallAll(prototype, property: string|symbol) {
    if (!prototype || typeof prototype !== 'object')
        return false

    if (!Object.prototype.hasOwnProperty.call(prototype, property))
        return canCallAll(Object.getPrototypeOf(prototype), property)

    const descriptor = Object.getOwnPropertyDescriptor(prototype, property)
    return typeof descriptor.value === 'function';
}